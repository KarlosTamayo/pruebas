﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(pruebas.web.Startup))]
namespace pruebas.web
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
